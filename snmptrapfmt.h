/*
 * snmptrapfmt.h
 *
 * Definitions for the snmptrapfmthdlr/snmptrapfmt pair.
 *
 *  Copyright (C) 2001 Uwe Maier <Uwe.Maier@nice.de>
 *  Copyright (C) 2001 - 2005 Bernd Schumacher Hewlett Packard Consulting
 *                            <bernd.schumacher@hp.com>
 *  Copyright (C) 2005 - 2009 Jochen Friedrich <jochen@scram.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU General Public License in the COPYING file at the
 *  root directory of this project for more details.
 *
 */

/*
 * The name of the communication fifo between the two processes.
 */
#define PIPENAME "/var/run/snmptrapfmt.p"
