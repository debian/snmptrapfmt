#! /bin/sh
#
### BEGIN INIT INFO
# Provides:          snmptrapfmt
# Required-Start:    $remote_fs
# Required-Stop:     $remote_fs
# Should-Start:      $network snmpd
# Should-Stop:       $network snmpd
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: SNMP Trap Formatting Agent
### END INIT INFO
#
# Written by Miquel van Smoorenburg <miquels@drinkel.ow.org>.
# Modified for Debian GNU/Linux by Ian Murdock <imurdock@gnu.ai.mit.edu>.
# Modified for Debian by Christoph Lameter <clameter@debian.org>

PATH=/bin:/usr/bin:/sbin:/usr/sbin
DAEMON=/usr/sbin/snmptrapfmt

. /lib/lsb/init-functions

# The following value is extracted by debstd to figure out how to generate
# the postinst script. Edit the field to change the way the script is
# registered through update-rc.d (see the manpage for update-rc.d!)
FLAGS="defaults 50"

test -f $DAEMON || exit 0

case "$1" in
  start)
    start-stop-daemon --start --verbose --exec $DAEMON -- -D -f /etc/snmp/snmptrapfmt.conf 
    ;;
  stop)
    start-stop-daemon --stop --verbose --exec $DAEMON -p /var/run/snmptrapfmt.pid
    ;;
  restart|reload|force-reload)
    start-stop-daemon --stop --verbose --exec $DAEMON -p /var/run/snmptrapfmt.pid
    start-stop-daemon --start --verbose --exec $DAEMON -- -D -f /etc/snmp/snmptrapfmt.conf
    ;;
  *)
    echo "Usage: /etc/init.d/snmptrapfmt {start|stop}"
    exit 1
    ;;
esac

exit 0
