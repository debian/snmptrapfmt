# Makefile for snmptrapfmt
#

TARGETS=	snmptrapfmthdlr snmptrapfmt
MANPAGES=	snmptrapfmthdlr.8 snmptrapfmt.8
CONFFILES=	snmptrapfmt.conf
DEFS=		-I /usr/include/ucd-snmp -DSTDC_HEADERS -DDEBUG

# Compilation parameters:

CFLAGS= -O2 -g -Wall $(DEFS) `dpkg-buildflags --get CFLAGS` `dpkg-buildflags --get CPPFLAGS`
LIBS= `dpkg-buildflags --get LDFLAGS` -lsnmp

# You should not need to change anything below this line.

all: $(TARGETS)

package: clean
	dpkg-buildpackage -us -uc

deb: clean
	debuild binary
  
snmptrapfmt:    snmptrapfmt.o log.o
	${CC} -g -o $@ snmptrapfmt.o log.o ${LIBS}

snmptrapfmthdlr:    snmptrapfmthdlr.o log.o
	${CC} -g -o $@ snmptrapfmthdlr.o log.o ${LIBS}

clean:
	rm -f core *.o *.bak ${TARGETS}

clobber: clean
	rm -f $(TARGETS)

install:
	make
	cp ${TARGETS} ${DESTDIR}/usr/sbin
	cp ${MANPAGES} ${DESTDIR}/usr/share/man/man8
	cp $(CONFFILES) $(DESTDIR)/etc/snmp
