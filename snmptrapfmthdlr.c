/*
 * snmptrapfmthdlr.c
 *
 * This is the trap handler which is called by 'snmptrapd'
 *
 * Usage: snmptrapfmthdlr [-d loglev]
 *
 *  Copyright (C) 2001 Uwe Maier <Uwe.Maier@nice.de>
 *  Copyright (C) 2001 - 2005 Bernd Schumacher Hewlett Packard Consulting
 *                            <bernd.schumacher@hp.com>
 *  Copyright (C) 2005 - 2009 Jochen Friedrich <jochen@scram.de>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  See the GNU General Public License in the COPYING file at the
 *  root directory of this project for more details.
 *
 */

/* --- import section --- */
#include "snmptrapfmt.h"
#include "log.h"

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/* --- local defines --- */
#define PNAME   "snmptrapfmthdlr"
#define LOGFILE "/var/tmp/snmptrapfmt.trc"

/* --- local types --- */

/* --- local variables --- */
int             o_loglev = 0;   /* the log level */

/* --- local functions --- */

/* -------------------------------------------------------------------------- */

int
main (int argc, char **argv)
{
  int             fd;           /* the handle for the pipe */
  char            inbuf[8192];  /* the read buffer */
  ssize_t         rcnt;         /* number of chars read from stdin */


  /*
   * Check the arguments 
   */
  if ((argc > 2) && (strcmp (argv[1], "-d") == 0))
  {
    o_loglev = atoi (argv[2]);
  }

  /* Init log file handling, append mode is on */
  loginit (PNAME, LOGFILE, o_loglev, 1);
  logging (1, "Traphandler started ...");

  /*
   * Check if the named pipe exists and a reader has opened. 
   * If not, the snmptrapfmt daemon does not run and this trap handlers
   * existence is obsolete ...
   */
  fd = open (PIPENAME, O_WRONLY | O_NONBLOCK);
  if (fd == -1)
  {
    logging (0, "Error opening pipe '%s' [%s]", PIPENAME, strerror (errno));
    exit (1);
  }
  logging (1, "Opened pipe '%s'", PIPENAME);

  /*
   * Read all data from stdin and forward to the pipe
   */
  while ((rcnt = read (0, inbuf, sizeof (inbuf))) > 0)
  {
    logging (1, "Read line from stdin: '%s'", inbuf);

    if (rcnt != write (fd, inbuf, rcnt))
    {
      logging (0, "Error writing to pipe '%s' [%s]", PIPENAME, strerror (errno));
      close (fd);

      exit (1);
    }
  }
  if (write (fd, "\n", 1) != 1)
  {
    logging (0, "Error writing EOD to pipe '%s' [%s]",
         PIPENAME, strerror (errno));
    close (fd);

    exit (1);
  }
  logging (1, "Wrote EOD to pipe");

  if (rcnt < 0)
  {
    logging (0, "Error reading from stdin [%s]", strerror (errno));
  }

  close (fd);

  logging (1, "Traphandler terminated.");

  return (rcnt == 0 ? 0 : 1);
}

/* -------------------------------------------------------------------------- */
